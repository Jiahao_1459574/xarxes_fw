/***************************************************************************
 *            fwClient.h
 *
 *  Copyright  2016  mc
 *  <mcarmen@<host>>
 ****************************************************************************/
 #include "fwClient.h"
  #include <sys/socket.h>
#include <arpa/inet.h>
 /**
  * Function that sets the field addr->sin_addr.s_addr from a host name
  * address.
  * @param addr struct where to set the address.
  * @param host the host name to be converted
  * @return -1 if there has been a problem during the conversion process.
  */
 int setaddrbyname(struct sockaddr_in *addr, char *host)
 {
   struct addrinfo hints, *res;
 	int status;

   memset(&hints, 0, sizeof(struct addrinfo));
   hints.ai_family = AF_INET;
   hints.ai_socktype = SOCK_STREAM;

   if ((status = getaddrinfo(host, NULL, &hints, &res)) != 0) {
     fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(status));
     return -1;
   }

   addr->sin_addr.s_addr = ((struct sockaddr_in *)res->ai_addr)->sin_addr.s_addr;

   freeaddrinfo(res);

   return 0;
 }


 /**
  * Returns the port specified as an application parameter or the default port
  * if no port has been specified.
  * @param argc the number of the application arguments.
  * @param an array with all the application arguments.
  * @return  the port number from the command line or the default port if
  * no port has been specified in the command line. Returns -1 if the application
  * has been called with the wrong parameters.
  */
 int getPort(int argc, char* argv[])
 {
   int param;
   int port = DEFAULT_PORT;

   optind=1;
   // We process the application execution parameters.
 	while((param = getopt(argc, argv, "h:p:")) != -1){
 		switch((char) param){
 		  case 'h': break;
 			case 'p':
 			  // We modify the port variable just in case a port is passed as a
 			  // parameter
 				port = atoi(optarg);
 				break;
 			default:
 				printf("Parametre %c desconegut\n\n", (char) param);
 				port = -1;
 		}
 	}

 	return port;
 }

 /**
  * Returns the host name where the server is running.
  * @param argc the number of the application arguments.
  * @param an array with all the application arguments.
  * @Return Returns the host name where the server is running.<br />
  * Returns null if the application has been called with the wrong parameters.
  */
  char * getHost(int argc, char* argv[]){
   char * hostName = NULL;
   int param;

   optind=1;
     // We process the application execution parameters.
 	while((param = getopt(argc, argv, "h:p:")) != -1){
 		switch((char) param){
 			case 'p': break;
 			case 'h':
         hostName = (char*) malloc(sizeof(char)*strlen(optarg)+1);
 				// Un cop creat l'espai, podem copiar la cadena
 				strcpy(hostName, optarg);
 				break;
 			default:
 				printf("Parametre %c desconegut\n\n", (char) param);
 				hostName = NULL;
 		}
 	}

 	printf("in getHost host: %s\n", hostName); //!!!!!!!!!!!!!!
 	return hostName;
  }



 /**
  * Shows the menu options.
  */
 void print_menu()
 {
 		// Mostrem un menu perque l'usuari pugui triar quina opcio fer

 		printf("\nAplicació de gestió del firewall\n");
 		printf("  0. Hello\n");
 		printf("  1. Llistar les regles filtrat\n");
 		printf("  2. Afegir una regla de filtrat\n");
 		printf("  3. Modificar una regla de filtrat\n");
 		printf("  4. Eliminar una regla de filtrat\n");
 		printf("  5. Eliminar totes les regles de filtrat.\n");
 		printf("  6. Sortir\n\n");
 		printf("Escull una opcio: ");
 }

/**
 * Check if ip adress is valid
 */
bool isValidIpAddress(char *ipAddress)
{
    struct sockaddr_in sa;
    int result = inet_pton(AF_INET, ipAddress, &(sa.sin_addr));
    return result != 0;
}

 /**
  * Sends a HELLO message and prints the server response.
  * @param sock socket used for the communication.
  */
 void process_hello_operation(int sock)
 {

   struct hello_rp hello_rp;
   //enviar msg
   char buff[2]={0};
   memset(buff,'\0',sizeof(buff));
   unsigned short opcode= MSG_HELLO;
   stshort(opcode, buff);

   send(sock, &buff, sizeof(buff), 0);

   //recibir msg
   recv(sock,&hello_rp,sizeof(hello_rp),0);
   /* diferenciar el opcode y el string que recibimos*/
   printf("%s",hello_rp.msg);

 }
 void process_list_operation(int sock){

     struct list_rp list_rp;
     char buff[2]={0};
     unsigned short opcode=MSG_LIST;
     stshort(opcode,buff);
     //enviem la petició
     send(sock,&buff,sizeof(buff),0);

     //rebem la petició
     recv(sock,&list_rp,sizeof(list_rp),0);


     int i=0;
     int num_rules=ntohs(list_rp.num_rules);
     printf("Regles de Forward\n");
     while(i<num_rules){
            char *ip=inet_ntoa(list_rp.rule[i].addr);
            char src_dst[4]={0};
            list_rp.rule[i].mask = ntohs(list_rp.rule[i].mask);
            list_rp.rule[i].port = ntohs(list_rp.rule[i].port);
            list_rp.rule[i].src_dst_addr = ntohs(list_rp.rule[i].src_dst_addr);
            list_rp.rule[i].src_dst_port = ntohs(list_rp.rule[i].src_dst_port);


            //cambiamos los numeros a caracteres
            char *auxSD=malloc(sizeof(char)*3);
            char *auxPort=malloc(sizeof(char)*5);
            if(list_rp.rule[i].src_dst_addr==0){
                auxSD="src";
            }
            if(list_rp.rule[i].src_dst_addr==1){
                auxSD="dst";
            }
            if(list_rp.rule[i].src_dst_addr==0){
                auxPort="sport";
            }
            if(list_rp.rule[i].src_dst_addr==1){
                auxPort="dport";
            }
            //printamos todo
            printf(" %i %s %s/%hu %s %hu\n",i+1,auxSD,inet_ntoa(list_rp.rule[i].addr), list_rp.rule[i].mask,auxPort,list_rp.rule[i].port);

            i++;
          }




 }

 void process_add_operation(int sock){
     struct add_rp add_rp;
     char *ip=malloc(sizeof(char)*17);
     unsigned short src_dst;
     unsigned short mask;
     unsigned short sdport;
     unsigned short port;
     char entradaSrc_dst[3];
     char entradaSDport[5];
     rule rule;
     printf("address src|dst Netmask [sport | dport] [port] \n");
     scanf("%s",&ip);
     //comprobamos si la ip es valida o no, si no, printamos el correspondiente error
     bool ipValido= isValidIpAddress(&ip);
     if(ipValido){

         printf("src|dst:\n");
         scanf("%s", entradaSrc_dst);
         //comprobamos si src/dst estan escritos de la forma correcta y ponemos la equivalencia
         //le asiganmos un 0 a src y un 1 a dst
         bool srcdstValido=FALSE;
         if( strcmp(entradaSrc_dst, "src") == 0) {
             srcdstValido=TRUE;
             src_dst=0;
         }
         if(  strcmp(entradaSrc_dst, "dst") == 0) {
             srcdstValido=TRUE;
             src_dst=1;
         }

         if(srcdstValido){
             printf("mask:\n");
             scanf("%hu",&mask);
             //comprobamos que la mascara este entre 0 y 32
             bool maskValido=FALSE;
             if((mask>=0)&&(mask<=32)){
                 maskValido=TRUE;
             }

             if(maskValido){

                 printf("sport|dport:\n");
                 scanf("%s",&entradaSDport);
                 //comprobamos si sport/dport estan escritos de la forma correcta y ponemos la equivalencia
                 //le asiganmos un 0 a sport y un 1 a dport
                 bool sdportValido=FALSE;
                 if( strcmp(entradaSDport, "sport") == 0) {
                     sdportValido=TRUE;
                     sdport=0;
                 }
                 if( strcmp(entradaSDport, "dport") == 0) {
                     sdportValido=TRUE;
                     sdport=1;
                 }

                 if(sdportValido){

                     printf("port\n");
                     scanf("%hu",&port);
                     bool portValido=FALSE;
                     if((port>=0)&&(port<=65535)){
                         portValido=TRUE;
                     }
                     if(portValido){

                         //Añadimos los datos en una rule para enviarlos
                         inet_aton(&ip,&rule.addr);
                         rule.src_dst_addr=htons(src_dst);
                         rule.mask=htons(mask);
                         rule.src_dst_port=htons(sdport);
                         rule.port=htons(port);
                         add_rp.opcode=htons(MSG_ADD);
                         add_rp.rule=rule;
                         char ipv[16];
                         strcpy(ipv,inet_ntoa(rule.addr));


                         //enviamos la rule
                         send(sock,&add_rp,sizeof(add_rp),0);

                         //recibimos la respuesta
                         struct list_rp list_rp;
                         recv(sock,&list_rp,sizeof(list_rp),0);
                         //hacemos lo msimo que en el list
                         int i=0;
                         int num_rules=ntohs(list_rp.num_rules);
                         printf("Regles de Forward\n");
                         while(i<num_rules){
                             char *ip=inet_ntoa(list_rp.rule[i].addr);
                             char src_dst[4]={0};
                             list_rp.rule[i].mask = ntohs(list_rp.rule[i].mask);
                             list_rp.rule[i].port = ntohs(list_rp.rule[i].port);
                             list_rp.rule[i].src_dst_addr = ntohs(list_rp.rule[i].src_dst_addr);
                             list_rp.rule[i].src_dst_port = ntohs(list_rp.rule[i].src_dst_port);


                             //cambiamos los numeros a caracteres
                             char *auxSD=malloc(sizeof(char)*3);
                             char *auxPort=malloc(sizeof(char)*5);
                             if(list_rp.rule[i].src_dst_addr==0){
                                 auxSD="src";
                             }
                             if(list_rp.rule[i].src_dst_addr==1){
                                 auxSD="dst";
                             }
                             if(list_rp.rule[i].src_dst_addr==0){
                                 auxPort="sport";
                             }
                             if(list_rp.rule[i].src_dst_addr==1){
                                 auxPort="dport";
                             }

                             printf(" %i %s %s/%hu %s %hu\n",i+1,auxSD,inet_ntoa(list_rp.rule[i].addr), list_rp.rule[i].mask,auxPort,list_rp.rule[i].port);

                             i++;
                         }
                        /* //recibimos la respuesta
                         unsigned short ok;
                         recv(sock,&ok,sizeof(unsigned short),0);
                         ok=ntohs(ok);

                         //cambiamos los numeros a caracteres
                         char *auxSD=malloc(sizeof(char)*3);
                         char *auxPort=malloc(sizeof(char)*5);
                         if(src_dst==0){
                             auxSD="src";
                         }
                         if(src_dst==1){
                             auxSD="dst";
                         }
                         if(sdport==0){
                             auxPort="sport";
                         }
                         if(sdport==1){
                             auxPort="dport";
                         }

                         //printamos por pantalla si se ha escrito correctamente la regla o no
                         if(ok==10){
                         printf("  La operació s'ha dut a terme correctament:\n");
                         printf(" [IP] %s   [MASK] %d   [SRC/DST] %s   [SPORT/DPORT] %s    [PORT]    %d\n",ipv,mask,auxSD,auxPort,ntohs(rule.port));




                         }else{
                            printf("Error al escriure la regla, aquesta regla ja existeix");
                         }*/

                     }else{//error en el port
                         printf("puerto no valido");
                     }



                 }else{//error en el sdport
                     printf("sport/dport no valido");

                 }

             }else{//error en el rango de mask
                 printf("mask no valido");

             }

         }else{//error al escribir dst o src
             printf("src/dst no valido");
         }

     }else{//error al escribir ip
         printf("ip no valida");

     }



 }




void process_change_operation(int sock){
  struct change_req change_req;
  change_req.opcode=htons(MSG_CHANGE);
    char entradaSrc_dst[3];
    char entradaSDport[5];

  unsigned short id;
  printf("rule id to change:\n");
  scanf("%hu",&id);
  change_req.id_rule=htons(id);

  rule rule;
  char *ip=malloc(sizeof(char)*17);
  unsigned short src_dst,mask,sdport,port;

  printf("test change operation\n");
  printf("ip:\n");
  scanf("%s",&ip);
  printf("src_dst:\n");
  scanf("%s",&entradaSrc_dst);
    if( strcmp(entradaSrc_dst, "src") == 0) {

        src_dst=0;
    }
    if(  strcmp(entradaSrc_dst, "dst") == 0) {

        src_dst=1;
    }
  printf("mask:\n");
  scanf("%hu",&mask);
  printf("src_dst_port:\n");
  scanf("%s",&entradaSDport);
    if( strcmp(entradaSDport, "sport") == 0) {

        sdport=0;
    }
    if( strcmp(entradaSDport, "dport") == 0) {

        sdport=1;
    }
  printf("port\n");
  scanf("%hu",&port);
  inet_aton(&ip,&rule.addr);
  rule.src_dst_addr=htons(src_dst);
  rule.mask=htons(mask);
  rule.src_dst_port=htons(sdport);
  rule.port=htons(port);
  change_req.rule=rule;

  send(sock,&change_req,sizeof(struct change_req),0);

  char msg[1024];
  char code[2];
  unsigned short opcode;
  recv(sock,&msg,sizeof(msg),0);
  memcpy(&code,&msg,sizeof(code));
  opcode=ldshort(code);
  if(opcode==MSG_OK){
    printf("ok!\n");
  }else if(opcode==MSG_ERR){
    printf("error!\n");
  }


}
void process_delete_operation(int sock){
  struct delete_req delete_req;
  delete_req.opcode=htons(MSG_DELETE);
  unsigned short id;
  printf("rule id to delete:\n");
  scanf("%hu",&id);
  delete_req.id_rule=htons(id);

  send(sock,&delete_req,sizeof(delete_req),0);

  char msg[1024];
  char code[2];
  unsigned short opcode;
  recv(sock,&msg,sizeof(msg),0);
  memcpy(&code,&msg,sizeof(code));
  opcode=ldshort(code);
  if(opcode==MSG_OK){
    printf("ok!\n");
  }else if(opcode==MSG_ERR){
    printf("error!\n");
  }


}
void process_flush_operation(int sock)
{
  unsigned short opcode=htons(MSG_FLUSH);
  send(sock,&opcode,sizeof(opcode),0);

  char msg[1024];
  char code[2];
  unsigned short op;
  recv(sock,&msg,sizeof(msg),0);
  memcpy(&code,&msg,sizeof(code));
  op=ldshort(code);
  if(op==MSG_OK){
    printf("ok!\n");
  }else if(op==MSG_ERR){
    printf("error!\n");
  }

}
 /**
  * Closes the socket connected to the server and finishes the program.
  * @param sock socket used for the communication.
  */
 void process_exit_operation(int sock)
 {
   //TO DO
   unsigned short op_code=MSG_FINISH;
   char buff[2]={0};
   stshort(op_code,buff);
   send(sock,&buff,sizeof(buff),0);

   exit(0);
 }

 /**
  * Function that process the menu option set by the user by calling
  * the function related to the menu option.
  * @param s The communications socket
  * @param option the menu option specified by the user.
  */
 void process_menu_option(int s, int option)
 {
   switch(option){
     // Opció HELLO
     case MENU_OP_HELLO:
       process_hello_operation(s);
       break;
     case MENU_OP_LIST_RULES:
     process_list_operation(s);
       break;
     case MENU_OP_ADD_RULE:
     process_add_operation(s);
       break;
     case MENU_OP_CHANGE_RULE:
     process_change_operation(s);
       break;
     case MENU_OP_DEL_RULE:
     process_delete_operation(s);
       break;
     case MENU_OP_FLUSH:
     process_flush_operation(s);
       break;
     case MENU_OP_EXIT:
     process_exit_operation(s);
     default:
       printf("Invalid menu option\n");
   }
 }


 int main(int argc, char *argv[]){
   int s;
   unsigned short port;
   char *hostName;
   int menu_option = 0;

printf("test 0\n");
   port = getPort(argc, argv);
   hostName = getHost(argc, argv);



 int s_cli = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
 struct sockaddr_in addr;
 addr.sin_family = AF_INET;
 addr.sin_port = htons(port);
 addr.sin_addr.s_addr = htonl(INADDR_ANY);
 connect(s_cli, (struct sockaddr*)&addr, sizeof(addr));

printf("test 1\n");


   //Checking that the host name has been set.Otherwise the application is stopped.
 	if(hostName == NULL){
 		perror("No s'ha especificat el nom del servidor\n\n");
 		return -1;
 	}


   do{
       print_menu();
 		  // getting the user input.
 		  scanf("%d",&menu_option);
 		  printf("\n\n");

 		  process_menu_option(s_cli, menu_option);

 	  }while(menu_option != MENU_OP_EXIT); //end while(opcio)

   return 0;
 }
