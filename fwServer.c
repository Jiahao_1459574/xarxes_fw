/***************************************************************************
 *            fwServer.c
 *
 *  Copyright  2016  mc
 *  <mc@<host>>
 ****************************************************************************/

#include "fwServer.h"
#include <string.h>

/**
 * Returns the port specified as an application parameter or the default port
 * if no port has been specified.
 * @param argc the number of the application arguments.
 * @param an array with all the application arguments.
 * @return  the port number from the command line or the default port if
 * no port has been specified in the command line. Returns -1 if the application
 * has been called with the wrong parameters.
 */
int getPort(int argc, char* argv[])
{
  int param;
  int port = DEFAULT_PORT;

  optind=1;
  // We process the application execution parameters.
	while((param = getopt(argc, argv, "p:")) != -1){
		switch((char) param){
			case 'p':
			  // We modify the port variable just in case a port is passed as a
			  // parameter
				port = atoi(optarg);
				break;
			default:
				printf("Parametre %c desconegut\n\n", (char) param);
				port = -1;
		}
	}

	return port;
}


 /**
 * Function that sends a HELLO_RP to the  client
 * @param sock the communications socket
 */
void process_HELLO_msg(int sock)
{
  printf("process hello\n");
  struct hello_rp hello_rp;
  char buffer[1024]={0};
  hello_rp.opcode=htons(MSG_HELLO_RP);
  strcpy(hello_rp.msg,"Hello World");

  send(sock,&hello_rp,sizeof(hello_rp),0);
}





void process_LIST_msg(int sock,struct FORWARD_chain *chain)
{
    unsigned short ok;
	struct list_rp list_rp;
	list_rp.opcode=htons(MSG_RULES);
    list_rp.num_rules=htons(chain->num_rules);
    struct fw_rule * node=chain->first_rule;

    int i=0;
    while(i<chain->num_rules && node!=NULL){
        list_rp.rule[i].addr.s_addr=node->rule.addr.s_addr;
        list_rp.rule[i].src_dst_addr=htons(node->rule.src_dst_addr);
        list_rp.rule[i].mask=htons(node->rule.mask);
        list_rp.rule[i].src_dst_port=htons(node->rule.src_dst_port);
        list_rp.rule[i].port=htons(node->rule.port);
        node=node->next_rule;
        i++;
    }

    send(sock,&list_rp,sizeof(list_rp),0);

}
void process_ADD_msg(int sock,struct FORWARD_chain *chain,struct add_rp* add_rp)
{

    printf("process add\n");
    struct fw_rule *node=NULL;
      unsigned short ok;

    //creamos y rellenamos la nueva rule
    struct fw_rule *new_rule= (struct fw_rule *) malloc(sizeof(struct fw_rule));
    new_rule->rule.addr.s_addr=add_rp->rule.addr.s_addr;
    new_rule->rule.src_dst_addr=ntohs(add_rp->rule.src_dst_addr);
    new_rule->rule.mask=ntohs(add_rp->rule.mask);
    new_rule->rule.src_dst_port=ntohs(add_rp->rule.src_dst_port);
    new_rule->rule.port=ntohs(add_rp->rule.port);
    new_rule->next_rule=NULL;

    //iteramos para buscar la posicion donde guardar la nueva rule
    chain->num_rules+=1;
    if(chain->first_rule==NULL){
        chain->first_rule=new_rule;
        process_LIST_msg(sock,chain);
    }else{


        node=chain->first_rule;
        while(node->next_rule!=NULL){
            node=node->next_rule;

        }
        node->next_rule=new_rule;
        process_LIST_msg(sock,chain);
    }

  //char *ip=inet_ntoa(new_rule->rule.addr);
  //printf("rule %d:%d %s/%d %d %d\n",ntohs(add_rp->opcode),new_rule->rule.src_dst_addr,ip,new_rule->rule.mask,new_rule->rule.src_dst_port,new_rule->rule.port);
  //  printf("rule %d:%d %s/%d %d %d\n",ntohs(add_rp->opcode),chain->first_rule->rule.src_dst_addr,inet_ntoa(new_rule->rule.addr),chain->first_rule->rule.mask,chain->first_rule->rule.src_dst_port,chain->first_rule->rule.port);
  //  ok=htons(MSG_OK);

  // send(sock,&ok,sizeof(unsigned short),0);

}
void process_CHANGE_msg(int sock,struct FORWARD_chain *chain,struct change_req* change_req){
  printf("process change\n");
  struct fw_rule *node=NULL;
  struct fw_rule *act=NULL;
  struct fw_rule *next=NULL;
  struct fw_rule *new_rule= (struct fw_rule *) malloc(sizeof(struct fw_rule));
  unsigned short ok;
  int i=0;
  int error=0;
  int id=htons(change_req->id_rule);
  node=chain->first_rule;

  new_rule->rule.addr.s_addr=change_req->rule.addr.s_addr;
  new_rule->rule.src_dst_addr=ntohs(change_req->rule.src_dst_addr);
  new_rule->rule.mask=ntohs(change_req->rule.mask);
  new_rule->rule.src_dst_port=ntohs(change_req->rule.src_dst_port);
  new_rule->rule.port=ntohs(change_req->rule.port);

  if(chain->first_rule==NULL){
      printf("error no rules\n");
      error=1;
  }else if(chain->num_rules==1){
      free(node);
      chain->first_rule=new_rule;
  }else{
      while(i<id-1 && node->next_rule!=NULL){
          node=node->next_rule;
          i++;
      }
      if(i==id-1){
        next=node->next_rule;
        if(next!=NULL) next=next->next_rule;
        act=node->next_rule;
        free(act);
        new_rule->next_rule=next;
        node->next_rule=new_rule;
      }

}

if(error!=1){
  ok=htons(MSG_OK);
  send(sock,&ok,sizeof(ok),0);
}else{
  struct error_rp error_rp;
  error_rp.opcode=htons(MSG_ERR);
  error_rp.errcode=htons(ERR_RULE);
  send(sock,&error_rp,sizeof(error_rp),0);
}
//send

}
void process_DELETE_msg(int sock,struct FORWARD_chain *chain,struct delete_req* delete_req){
  printf("process delete\n");
  unsigned short ok;
  unsigned short id=ntohs(delete_req->id_rule);
  int i=0;
  int error=0;
  struct fw_rule *node=NULL;
  struct fw_rule *act=NULL;
  struct fw_rule *next=NULL;
  if(chain->num_rules==0){
    printf("no rules\n");
    error=1;
  }else if(id==0){

    node=chain->first_rule;
    node=node->next_rule;
    free(chain->first_rule);
    chain->first_rule=node;
    chain->num_rules--;
  }else{
    node=chain->first_rule;
    while(i<id-1 && node->next_rule!=NULL){
        node=node->next_rule;
        i++;
    }
    if(i==id-1){

      next=node->next_rule;
      act=node->next_rule;
      if(next->next_rule!=NULL)
      { next=next->next_rule;
        free(act);
        node->next_rule=next;
      }else{
        free(act);
        node->next_rule=NULL;
      }
      chain->num_rules--;

    }
  }

  if(error!=1){
    ok=htons(MSG_OK);
    send(sock,&ok,sizeof(ok),0);
  }else{
    struct error_rp error_rp;
    error_rp.opcode=htons(MSG_ERR);
    error_rp.errcode=htons(ERR_RULE);
    send(sock,&error_rp,sizeof(error_rp),0);
  }

}

void process_FLUSH_msg(int sock,struct FORWARD_chain *chain){
  int n=chain->num_rules;
  struct fw_rule *node=chain->first_rule;
  struct fw_rule *next=NULL;
    while(n>0){
    next=node->next_rule;
    free(node);
    node=next;
    chain->first_rule=node;
    n--;
    chain->num_rules--;
   }
  unsigned short ok=htons(MSG_OK);
  send(sock,&ok,sizeof(ok),0);

}
 /**
 * Receives and process the request from a client.
 * @param the socket connected to the client.
 * @param chain the chain with the filter rules.
 * @return 1 if the user has exit the client application therefore the
 * connection whith the client has to be closed. 0 if the user is still
 * interacting with the client application.
 */
int process_msg(int sock, struct FORWARD_chain *chain)
{
  unsigned short op_code;
  struct delete_req delete_req;
  struct change_req change_req;
  struct add_rp add_rp;

  int finish = 0;
  char msg[1024];
  recv(sock,&msg,sizeof(msg),0);
  //mensaje que recibimos diferenciar el opcode y el cuerpo de datos
  char code[2];
  memcpy(&code,&msg,2);
  op_code=ldshort(code);
  //sleep(100);
  switch(op_code)
  {
    case MSG_HELLO:
    process_HELLO_msg(sock);
      break;
    case MSG_LIST:
    process_LIST_msg(sock,chain);
      break;
    case MSG_ADD:

    memcpy(&add_rp,&msg,sizeof(struct add_rp));
    process_ADD_msg(sock,chain,&add_rp);
      break;
    case MSG_CHANGE:

    memcpy(&change_req,&msg,sizeof(struct change_req));
    process_CHANGE_msg(sock,chain,&change_req);
      break;
    case MSG_DELETE:

    memcpy(&delete_req,&msg,sizeof(struct delete_req));
    process_DELETE_msg(sock,chain,&delete_req);
      break;
    case MSG_FLUSH:
    process_FLUSH_msg(sock,chain);

      break;
    case MSG_FINISH:

      //TO DO
      close(sock);
      finish = 1;
      break;
    default:
      perror("Message code does not exist.\n");
  }

  return finish;
}




 int main(int argc, char *argv[]){
  int port = getPort(argc, argv);
  int finish=0;
  int s2;
  struct FORWARD_chain chain;

  chain.num_rules=0;
  chain.first_rule=NULL;


int s_serv = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
struct sockaddr_in addr;
addr.sin_family = AF_INET;
addr.sin_port = htons(port);
addr.sin_addr.s_addr = htonl(INADDR_ANY);
socklen_t addrlen=sizeof(addr);


bind(s_serv, (struct sockaddr*)&addr, sizeof(addr));

listen(s_serv, 10);



int i;
int pid[10];
for(i=0;i<10;i++){

  if(fork()==0){
    //child process
    while(1) {
    printf("esperar cliente\n");
    s2=accept(s_serv,(struct sockaddr *)&addr,&addrlen);
    printf("cliente %d conectado\n",i);
      //TO DO
      do {
        finish=process_msg(s2,&chain);
      }while(!finish);

    }
    exit(0);
  }
}

for(i=0;i<10;i++){
  //parent process
  wait(NULL);
}


  return 0;
 }
